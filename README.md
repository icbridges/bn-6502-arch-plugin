# Binary Ninja 6502 Plugin
## Build
* Only tested on Linux x64
* Build with `make` in the top level directory
  * You'll probably need to update the paths in both Makefile and CMakeLists.txt for your environment
## Install
* Copy the compiled library (in the bin/ directory) to your Binary Ninja 
plugins directory
* Run Binary Ninja
* Use the Binary Ninja plugin manager to enable the plugin
## Testing
* This plugin was tested with the following CSAW CTF NES challenge: https://github.com/Vector35/PwnAdventureZ 
